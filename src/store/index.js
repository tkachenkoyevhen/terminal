import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    users: [
      {
        id: 1,
        name: 'Johm Smith',
        sex: 'male',
        age: 30
      },
      {
        id: 2,
        name: 'Jenifer Lopez',
        sex: 'female',
        age: 45
      },
      {
        id: 3,
        name: 'Jim Carrey',
        sex: 'male',
        age: 50
      },
      {
        id: 4,
        name: 'Tom Kruz',
        sex: 'male',
        age: 40
      }
    ],
    rates: [10, 20, 50, 100, 200, 500],
    cash: [
      {
        id: 1,
        rate: [20, 20, 20, 20, 20, 20]
      },
      {
        id: 2,
        rate: [20, 20, 20, 20, 20, 20]
      },
      {
        id: 3,
        rate: [20, 20, 20, 20, 20, 20]
      },
      {
        id: 4,
        rate: [20, 20, 20, 20, 20, 20]
      }
    ],
    totalCash: 0
  },
  mutations: {

    totalCash (state, payload) {
      const total = state.cash.find((el) => el.id == payload).rate
        .map((el, index) => {
          return state.rates[index] * el
        }).reduce((a, b) => a + b, 0)

      Vue.set(state, 'totalCash', total)
    },
    getCash (state, payload) {
      let needCash = parseInt(payload.amount)
      // const numOfnull = needCash.toString().replace(/[^0]*/g, '').length
      // console.log('numOfnull', numOfnull)

      const cash = Object.assign({}, state.cash.find((cash) => cash.id == payload.id).rate)
      const rates = Object.assign([], state.rates)

      const suitAbleRate = rates.map((rate, index) =>  {
        return (cash[index] > 0) ? Math.floor(needCash / rate) : Infinity
      })
      const indSuitAble = suitAbleRate.indexOf(Math.min.apply(null, suitAbleRate))

      for (let i = indSuitAble; rates[i]; --i) {
        for (let k = cash[i]; k >= 0; --k) {
          needCash -= rates[i]
          if (needCash >= 0) {
            --cash[i]
          }
        }
      }
      state.cash.find((cash) => cash.id == payload.id).rate = Object.assign([], cash)
    }

  },
  actions: {
    getCash({commit}, amount) {
      commit('getCash', amount)
    }
  },
  modules: {
  }
})
